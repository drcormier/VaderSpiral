import configparser
from os import path
import math
import random
import numpy as np

CONFIGFILENAME = 'config.ini'
VALUES = 'VALUES'
PROG = 'ProgramOptions'
CC = 'ColumnCenters'


def default_config_write(filename: str):
    config = configparser.ConfigParser()
    config[VALUES] = {
        'ai': '3.175',
        'b': '1.1117',
        'zs': '23.43',
        'w': '18',
        'n': '5',
        'feedrate': '10000',
        'zheight': '0.15',
        'numlayers': '1'
    }
    config[PROG] = {
        'OutputFileName': 'spiral.gcode',
        'ccwSpiral': True,
        'cwSpiral': False,
        'randomStartPoint': True,
        'printalllayers': False
    }
    config[CC] = {
        'columns': '0, 0, 0, 0'
    }
    with open(filename, 'w') as f:
        config.write(f)
    return config


def config_read(filename: str):
    config = configparser.ConfigParser()
    config.read(filename)
    return config


def config_parse(config):
    ret = {}
    for key, value in config.items(VALUES):
        ret[key] = float(value)
    return ret


def centers_parse(config):
    ret = []
    for loc in config[CC]['columns'].split('\n'):
        loc = loc.split(',')
        x = loc[0].strip()
        y = loc[1].strip()
        xskew = loc[2].strip()
        yskew = loc[3].strip()
        ret.append((float(x), float(y), float(xskew), float(yskew)))
    return ret


def headers() -> str:
    header = ''
    header += 'G64\n'
    header += 'PRIO_OFF\n'
    header += 'G57\n'
    header += 'PRIO_PULSE_DISTANCE=CALIBRATION_MASS/27/TRACK_AREA\n'
    header += 'PRIO_ON_PATH_OFFSET = (0.1/$MN_INT_INCR_PER_MM)\n'
    return header


def footers() -> str:
    return 'M30\n'


def g0(x: float, y: float, z: float) -> str:
    return 'G0 X{:.4f} Y{:.4f} Z{:.4f}\n'.format(x, y, z)


def g1(x: float, y: float, z: float, f: int) -> str:
    return 'G1 X{:.4f} Y{:.4f} Z{:.4f} F{:d}\n'.format(x, y, z, f)


def g2(x: float, y: float, i: int, j: int, f: int) -> str:
    return 'G2 X{:.4f} Y{:.4f} I{:d} J{:d} F{:d}\n'.format(x, y, i, j, f)


def g3(x: float, y: float, i: float, j: float, f: int) -> str:
    return 'G3 X{:.4f} Y{:.4f} I{:.4f} J{:.4f} F{:d}\n'.format(x, y, i, j, f)


def pon() -> str:
    return 'PRIO_ON\n'


def poff() -> str:
    return 'PRIO_OFF\n'


def ppd() -> str:
    return 'PRIO_PULSE_DISTANCE=CALIBRATION_MASS/27/TRACK_AREA\n'


def pirange():
    l = np.arange(0, 2 * math.pi, 0.1)
    random.shuffle(l)
    for i in l:
        yield i


def spiral(mapping: dict, cw: bool, layer: int, rand: bool, idist: float, odist: float, xorigin: float, yorigin: float, xskew: float, yskew: float):
    """

    :param file: File that is written to
    :param mapping: Gets values from config file
    :param cw: True if spiral prints clockwise, False if counterclockwise
    :param layer: Number of layers in cylinder
    :param rand: True if start of layer is randomized, False otherwise
    :param idist: Distance from center of spiral to initial point
    :param odist: Distance from the center of spiral to the start point of the last revolution in that same layer
    :return:
    """
    ret = ''
    ai = mapping.get('ai')
    b = mapping.get('b')
    zs = mapping.get('zs')
    w = mapping.get('w')
    n = mapping.get('n')
    n = int(n)
    w = int(w)
    feedrate = mapping.get('feedrate')
    feedrate = int(feedrate)
    zheight = zs + layer * mapping.get('zheight')
    llx = (layer-1)*xskew + xorigin
    lly = (layer-1)*yskew + yorigin
    lx = layer*xskew + xorigin
    ly = layer*yskew + yorigin
    innerdistance = 0
    distance2 = 0
    randstart = 0
    p = pirange()
    # get random starting point
    if rand:
        randstart = random.uniform(0, 2 * math.pi)
    R0 = randstart
    while layer > 0:
        try:
            randstart = next(p)
        except StopIteration:
            print("ERROR: failed to find valid start point on layer {}, setting start point to 0...".format(layer))
            randstart = 0
            break
        R0 = randstart
        X0 = ((ai + b * (R0-randstart)) * math.cos(R0)) + layer*xskew + xorigin
        Y0 = ((ai + b * (R0-randstart)) * math.sin(R0)) + layer*yskew + yorigin

        distance = math.sqrt((X0 - llx)**2 + (Y0 - lly)**2)
        # if our first point on new layer is NOT over the open space (its in shared area), then leave the loop
        if idist <= distance < odist:
            break
    # print n turns
    for i in range(0, n):
        # move to initial location
        R0 = ((i * w) * 2 * math.pi / w) + randstart
        X0 = ((ai + b * (R0 - randstart)) * math.cos(R0)) + layer * xskew + xorigin
        Y0 = ((ai + b * (R0 - randstart)) * math.sin(R0)) + layer * yskew + yorigin
        if i == 0:
            innerdistance = math.sqrt(((X0 - lx)**2) +((Y0 - ly)**2))
        if i == n-1:
            outerdistance = math.sqrt(((X0 - lx)**2) +((Y0 - ly)**2))
        if cw:
            X0 = -X0
            Y0 = -Y0
        ret += g0(X0, Y0, zheight)
        # start printing
        ret += pon()
        # each turn is broken up into w segments
        for j in range(0, w+1):
            R0 = (((i * w) + j) * 2 * math.pi / w) + randstart

            X1 = ((ai + b * (R0 - randstart)) * math.cos(R0)) + layer*xskew + xorigin
            Y1 = ((ai + b * (R0 - randstart)) * math.sin(R0)) + layer*yskew + yorigin

            if cw:
                X1 = -X1
                Y1 = -Y1

            ret += g1(X1, Y1, zheight, feedrate)
        ret += poff()
    return ret, innerdistance, outerdistance


def column(mapping: dict, cw: bool, rand: bool, xorigin: float, yorigin: float, xskew: float, yskew: float):
    """

    :param file: File that is written to
    :param mapping: Gets values from config file
    :param cw: True if spiral prints clockwise, False if counterclockwise
    :param rand: True if start of layer is randomized, False otherwise
    :return:
    """
    numlayers = mapping.get('numlayers')
    numlayers = int(numlayers)
    inner = 0
    outer = 0
    for i in range(0, numlayers):
        data, inner, outer = spiral(mapping, cw, i, rand, inner, outer, xorigin, yorigin, xskew, yskew)
        # file.write(";finished layer {}\n".format(i))
        yield data


def main():
    if not path.isfile(CONFIGFILENAME):
        default_config_write(CONFIGFILENAME)
    c = config_read(CONFIGFILENAME)
    mapping = config_parse(c)
    fname = c[PROG]['outputfilename']
    with open(fname, 'w+') as f:
        f.write(headers())
        ccw = c.getboolean(PROG, 'ccwspiral')
        cw = c.getboolean(PROG, 'cwspiral')
        rand = c.getboolean(PROG, 'randomstartpoint')
        column_list = []
        if ccw:
            for col in centers_parse(c):
                x, y, xskew, yskew = col
                column_list.append(column(mapping, False, rand, x, y, xskew, yskew))
            if c.getboolean(PROG, "printalllayers"):
                for i in range(0, int(mapping.get("numlayers"))):
                    for col in column_list:
                            f.write(next(col))
            else:
                for col in column_list:
                    for layer in col:
                        f.write(layer)
        if cw:
            column(mapping, True, rand)
        f.write(footers())


if __name__ == '__main__':
    main()
